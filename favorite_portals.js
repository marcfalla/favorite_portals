// ==UserScript==
// @id             iitc-plugin-favorite-portals@Mushovelik
// @name           IITC plugin: Favorite portals
// @author         Mushovelik
// @category       Controls
// @version        0.1
// @description    Bookmark your favorite portals and display portals from our community
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// @grant          none
// ==/UserScript==

function wrapper(plugin_info) {
// ensure plugin framework is there, even if iitc is not yet loaded
if (typeof window.plugin !== 'function') window.plugin = function() {};

// PLUGIN START ////////////////////////////////////////////////////////
/***********************************************************************

  HOOKS:
  - pluginBkmrksEdit: fired when a bookmark is removed, added or sorted, also when a folder is opened/closed;

***********************************************************************/
////////////////////////////////////////////////////////////////////////

  //ensure we load the setup once
  var timerLaunch;
  var setupHasBeenLaunched = false;
  var timerParseIntervalId = 0;

  // use own namespace for plugin
  window.plugin.favoritePortals = function() {};
  window.plugin.favoritePortals.allPortals;
  window.plugin.favoritePortals.allPortalsHighlighters = [];

  window.plugin.favoritePortals.KEY_STORAGE = 'plugin-favorite-portals';
  window.plugin.favoritePortals.KEY_STATUS_BOX = 'plugin-favorite-portals-box';

  window.plugin.favoritePortals.bkmrksObj = {};
  window.plugin.favoritePortals.statusBox = {};

  window.plugin.favoritePortals.starLayers = {};
  window.plugin.favoritePortals.starLayerGroup = null;
  window.plugin.favoritePortals.playerName = window.PLAYER.nickname;

/*********************************************************************************************************************/

  // Generate an ID for the bookmark (date time + random number)
  window.plugin.favoritePortals.generateID = function() {
    var d = new Date();
    var ID = 'id' + (d.getTime() + (Math.floor(Math.random()*99)+1)).toString();
    return ID;
  };

  // Update the localStorage
  window.plugin.favoritePortals.saveStorage = function(action, fields) {
    localStorage[plugin.favoritePortals.KEY_STORAGE] = JSON.stringify(window.plugin.favoritePortals.bkmrksObj);

    if (action == 'create') {
      // get the portal
      var Portals = Parse.Object.extend("portals");
      var portal = new Portals();
      portal.set("player_name", window.plugin.favoritePortals.playerName);
      portal.set("name", fields['name']);
      portal.set("lat", String(fields['lat']));
      portal.set("lng", String(fields['lng']));
      portal.set("team", (window.PLAYER.team == "ENLIGHTENED") ? 2 : 1);
      portal.set('guid', fields['guid']);
      //query.set("type", fields['type'])

      for (var i=0; i < fields['zones'].length; i++) {
        portal.set(fields['zones'][i]['key'], fields['zones'][i]['value']);
      }

      portal.save();
      window.plugin.favoritePortals.addPortalHighlight(portal);

    } else if (action == 'update') {

      // get the portal
      var Portals = Parse.Object.extend("portals");
      var query = new Parse.Query(Portals);
      query.equalTo("player_name", window.plugin.favoritePortals.playerName);
      query.equalTo("deletedAt", null);
      query.equalTo("lat", String(fields['lat']));
      query.equalTo("lng", String(fields['lng']));
      query.first({
        success: function(portal) {
          for (var i=0; i < fields['zones'].length; i++) {
            portal.set(fields['zones'][i]['key'], fields['zones'][i]['value']);
          }
          portal.save();

          window.plugin.favoritePortals.deleteMarker(portal);
          window.plugin.favoritePortals.addPortalHighlight(portal);
        }
      });

    } else if (action == 'delete') {
      // get the portal
      var Portals = Parse.Object.extend("portals");
      var query = new Parse.Query(Portals);
      query.equalTo("player_name", window.plugin.favoritePortals.playerName);
      query.equalTo("deletedAt", null);
      query.equalTo("lat", String(fields['lat']));
      query.equalTo("lng", String(fields['lng']));
      query.first({
        success: function(portal) {
          portal.set("deletedAt", new Date());
          portal.save();

          window.plugin.favoritePortals.deleteMarker(portal);
        }
      });

    }
  }

  // Load the localStorage
  window.plugin.favoritePortals.loadStorage = function(syncFromServer) {
    window.plugin.favoritePortals.bkmrksObj = JSON.parse(localStorage[plugin.favoritePortals.KEY_STORAGE]);

    if (syncFromServer) {
      var level, folder, guid, p, ll, label;

      var Portals = Parse.Object.extend("portals");
      var query = new Parse.Query(Portals);
      // query.equalTo("player_name", window.plugin.favoritePortals.playerName);
      query.equalTo("deletedAt", null);
      query.limit(1000);
      query.find({
        success: function(results) {
          window.plugin.favoritePortals.allPortals = results;
          window.plugin.favoritePortals.DisplayAvailablePortals();
          window.plugin.favoritePortals.addPortalsHighlights();
        },
        error: function(error) {
          console.log("Error: " + error.code + " " + error.message);
        }
      });
    }
  }

  window.plugin.favoritePortals.DisplayAvailablePortals = function() {
    allPortals = window.plugin.favoritePortals.allPortals;
    allPortalsCount = allPortals.length;

    for (var i = 0; i < allPortalsCount; i++) {
      obj = allPortals[i];
      player_name = obj.get('player_name');

      // display my favorite portal
      if (player_name == window.plugin.favoritePortals.playerName) {
        guid    = obj.get('guid');
        level   = obj.get('primary') ? 1 : (obj.get('secondary') ? 2 : 3);
        obj_lat = obj.get('lat');
        obj_lng = obj.get('lng');
        label   = obj.get('name');

        window.plugin.favoritePortals.addPortalBookmark(null, guid, level, obj_lat + ',' + obj_lng, label);
      }
    }
  }

  window.plugin.favoritePortals.addPortalHighlight = function(portal) {
    var guid   = portal.get('guid');
    var lat    = portal.get('lat');
    var lng    = portal.get('lng');
    var portalLevel = portal.get('primary') ? 1 : (portal.get('secondary') ? 2 : 3);
    var color  = (portal.get('team') == 2) ? '#03fe03' : '#00c5ff'

    var zoom = map.getZoom();
    zoomScale = zoom >= 16 ? 1 : zoom >= 14 ? 0.7 : zoom >= 10 ? 0.5 : 0.4;
    var radius = ((1 + 3 - portalLevel) * 12 * zoomScale);
    var options = {
      radius: radius,
      stroke: false,
      color: color,
      weight: 1,
      opacity: 1,
      fill: true,
      fillColor: color,
      fillOpacity:'0.' + (6 - portalLevel),
      dashArray: null
    };

    // console.log(marker_options);
    // portal.setStyle(markerOptions);

    var options = L.extend({}, options, options, { clickable: true });
    var latlng = new L.LatLng(lat, lng);
    var marker = L.circleMarker(latlng, options);
    window.plugin.favoritePortals.allPortalsHighlighters.push(marker);

    marker.addTo(map);

    (function(guid) {
      marker.on('click', function(e) {
        window.renderPortalDetails(guid);
      });
    }(guid));
  }

  window.plugin.favoritePortals.addPortalsHighlights = function() {
    allPortals = window.plugin.favoritePortals.allPortals;
    allPortalsCount = allPortals.length;

    for (var i = 0; i < allPortalsCount; i++) {
      window.plugin.favoritePortals.addPortalHighlight(allPortals[i]);
    }
  }

  window.plugin.favoritePortals.deleteAllMarkers = function() {
    var allPortals = window.plugin.favoritePortals.allPortals;
    var allPortalsCount = allPortals.length;

    for (var i = 0; i < allPortalsCount; i++) {
      window.plugin.favoritePortals.deleteMarker(allPortals[i]);
    }
  }

  window.plugin.favoritePortals.deleteMarker = function(portal) {
    markerIndex = window.plugin.favoritePortals.findMarkerIndexByLatLng(portal.get('lat'), portal.get('lng'));
    markers = window.plugin.favoritePortals.allPortalsHighlighters;
    markers[markerIndex].onRemove(map);
    markers.splice(markerIndex, 1);
  }

  window.plugin.favoritePortals.findMarkerIndexByLatLng = function(lat, lng) {
    markers = window.plugin.favoritePortals.allPortalsHighlighters;
    markersCount = markers.length;
    for (var i = 0; i < markersCount; i++) {
      var marker = markers[i];
      var markerLatlng = marker.getLatLng();
      if ((markerLatlng.lat == lat) && (markerLatlng.lng == lng)) {
        return i
      }
    }
  }

  window.plugin.favoritePortals.findGuid = function() {
    window.plugin.favoritePortals.saveStorage();
    for (var _guid in window.portals) {
      p = window.portals[_guid];
      ll = p.getLatLng();

      if (ll.lat == obj_lat && ll.lng == obj_lng) {
        return _guid
        break;
      }
    }
  }

  window.plugin.favoritePortals.saveStorageBox = function() {
    localStorage[plugin.favoritePortals.KEY_STATUS_BOX] = JSON.stringify(window.plugin.favoritePortals.statusBox);
  }
  window.plugin.favoritePortals.loadStorageBox = function() {
    window.plugin.favoritePortals.statusBox = JSON.parse(localStorage[plugin.favoritePortals.KEY_STATUS_BOX]);
  }

  window.plugin.favoritePortals.createStorage = function() {
    window.plugin.favoritePortals.bkmrksObj.portals = {"id1":{"label":"primary","state":0,"bkmrk":{}},"id2":{"label":"secondary","state":0,"bkmrk":{}},"id3":{"label":"terciary","state":0,"bkmrk":{}}};
    window.plugin.favoritePortals.saveStorage();

    window.plugin.favoritePortals.statusBox.show = 1;
    window.plugin.favoritePortals.statusBox.page = 0;
    window.plugin.favoritePortals.statusBox.pos = {x:100,y:100};
    window.plugin.favoritePortals.saveStorageBox();
  }

  window.plugin.favoritePortals.refreshBkmrks = function() {
    $('#bkmrk_portals > ul').remove();

    window.plugin.favoritePortals.loadStorage();
    window.plugin.favoritePortals.loadList();

    window.plugin.favoritePortals.updateStarsPortal();
    window.plugin.favoritePortals.jquerySortableScript();
  }

/***************************************************************************************************************************************************************/

  // Show/hide the bookmarks box
  window.plugin.favoritePortals.switchStatusBkmrksBox = function(status) {
    var newStatus = status;

    if (newStatus === 'switch') {
      if (window.plugin.favoritePortals.statusBox.show === 1) {
        newStatus = 0;
      } else {
        newStatus = 1;
      }
    }

    if (newStatus === 1) {
      $('#bookmarksBox').css('height', 'auto');
      $('#bkmrksTrigger').css('height', '0');
    } else {
      $('#bkmrksTrigger').css('height', '64px');
      $('#bookmarksBox').css('height', '0');
    }

    window.plugin.favoritePortals.statusBox['show'] = newStatus;
    window.plugin.favoritePortals.saveStorageBox();
  }

  // Switch the status folder to open/close (in the localStorage)
  window.plugin.favoritePortals.openFolder = function(elem) {
    $(elem).parent().parent('li').toggleClass('open');

    var ID = $(elem).parent().parent('li').attr('id');
    var newFlag;
    var flag = window.plugin.favoritePortals.bkmrksObj.portals[ID]['state'];

    if (flag) { newFlag = 0; }
    else if (!flag) { newFlag = 1; }

    window.plugin.favoritePortals.bkmrksObj.portals[ID]['state'] = newFlag;
    window.plugin.favoritePortals.saveStorage();
    window.runHooks('pluginBkmrksEdit', { "target": "folder", "action": newFlag ? "open":"close", "id": ID });
  }

  // Load the HTML bookmarks
  window.plugin.favoritePortals.loadList = function() {
    var element = '';
    var elementTemp = '';
    var elementExc = '';
    var returnToMap = '';

    // For each folder
    var list = window.plugin.favoritePortals.bkmrksObj.portals;

    for(var idFolders in list) {
      var folders = list[idFolders];
      var active = '';

      // Create a label and a anchor for the sortable
      var folderLabel = '<span class="folderLabel"><a class="bookmarksAnchor" onclick="window.plugin.favoritePortals.openFolder(this);return false"><span></span>' + folders['label'] + '</a></span>';

      if (folders['state']) { active = ' open'; }
      // Create a folder
      elementTemp = '<li class="bookmarkFolder' + active + '" id="' + idFolders + '">' + folderLabel + '<ul>';

      // For each bookmark
      var fold = folders['bkmrk'];
      for(var idBkmrk in fold) {
        var btn_link;
        var btn_remove = '<a class="bookmarksRemoveFrom" onclick="window.plugin.favoritePortals.removeElement(this);return false;" title="Remove from bookmarks">X</a>';

        var btn_move = '';

        var bkmrk = fold[idBkmrk];
        var label = bkmrk['label'];
        var latlng = bkmrk['latlng'];

        var guid = bkmrk['guid'];
        var btn_link = '<a class="bookmarksLink" onclick="$(\'a.bookmarksLink.selected\').removeClass(\'selected\');' + returnToMap + 'window.zoomToAndShowPortal(\'' + guid + '\', [' + latlng + ']);return false;">' + label + '</a>';

        // Create the bookmark
        elementTemp += '<li class="bkmrk" id="' + idBkmrk + '">' + btn_remove+btn_move+btn_link + '</li>';
      }
      elementTemp += '</li></ul>';

      element += elementTemp;
    }
    element = '<ul>' + element + '</ul>';

    // Append all folders and bookmarks
    $('#bkmrk_portals').append(element);
  }

/***************************************************************************************************************************************************************/

  window.plugin.favoritePortals.findByGuid = function(guid) {
    var list = window.plugin.favoritePortals.bkmrksObj.portals;

    for(var idFolders in list) {
      for(var idBkmrk in list[idFolders]['bkmrk']) {
        var portalGuid = list[idFolders]['bkmrk'][idBkmrk]['guid'];
        if (guid === portalGuid) {
          return {"id_folder":idFolders,"id_bookmark":idBkmrk};
        }
      }
    }
    return;
  }

  // Append 'stars' flag in sidebar.
  window.plugin.favoritePortals.onPortalSelected = function() {
    $('.bkmrksStar').remove();

    if (window.selectedPortal == null) return;

    setTimeout(function() { // the sidebar is constructed after firing the hook
      if (typeof(Storage) === "undefined") {
        $('#portaldetails > .imgpreview').after(plugin.favoritePortals.htmlDisabledMessage);
        return;
      }

      $('#portaldetails > h3.title').before(plugin.favoritePortals.htmlStars);
      window.plugin.favoritePortals.updateStarsPortal();
    }, 0);
  }

   var getPortalKeyNameFromLevel = function(level) {
    if (level == 1) {
      return 'primary';
    } else if (level == 2) {
      return 'secondary';
    } else {
      return 'terciary';
    }
  }

  // Update the status of the star (when a portal is selected from the map/bookmarks-list)
  window.plugin.favoritePortals.updateStarsPortal = function() {
    var guid = window.selectedPortal;
    $('.bkmrksStar').removeClass('favorite');
    $('.bkmrk a.bookmarksLink.selected').removeClass('selected');

    // If current portal is into bookmarks: select bookmark portal from portals list and select the star
    if (localStorage[window.plugin.favoritePortals.KEY_STORAGE].search(guid) != -1) {
      var bkmrkData = window.plugin.favoritePortals.findByGuid(guid);
      if (bkmrkData) {
        $('.bkmrk#' + bkmrkData['id_bookmark'] + ' a.bookmarksLink').addClass('selected');
        // console.log(Number(bkmrkData['id_folder'].slice(2)));
        var index = 3 - Number(bkmrkData['id_folder'].slice(2)) + 1;
        // console.log(index)
        $('.bkmrksStar').slice(0, index).addClass('favorite');
      }
    }
  }

  // Switch the status of the star
  window.plugin.favoritePortals.switchStarPortal = function(level, guid) {
    if (guid == undefined) guid = window.selectedPortal;

    var bookmarkAction;

    // If portal is saved in bookmarks
    var bkmrkData = window.plugin.favoritePortals.findByGuid(guid);
    if (bkmrkData) {

      // remove this bookmark
      bookmarkAction = 'delete'
      var list = window.plugin.favoritePortals.bkmrksObj.portals;
      var item =list[bkmrkData['id_folder']]['bkmrk'][bkmrkData['id_bookmark']];
      var latlng = item.latlng.split(',');
      delete list[bkmrkData['id_folder']]['bkmrk'][bkmrkData['id_bookmark']];

      $('.bkmrk#' + bkmrkData['id_bookmark'] + '').remove();

      window.plugin.favoritePortals.saveStorage();
      window.plugin.favoritePortals.updateStarsPortal();

      window.runHooks('pluginBkmrksEdit', {"target": "portal", "action": "remove", "folder": bkmrkData['id_folder'], "id": bkmrkData['id_bookmark'], "guid":guid});

      currentPortalLevel = Number(bkmrkData['id_folder'].slice(2));
      // if the level clicked is not the same as the level in bookmark
      // Add this bookmark to the new folder
      if (currentPortalLevel != level) {
        bookmarkAction = 'update';

      } else {
         bookmarkAction = 'delete';
         var fields = {
          'lat': latlng[0],
          'lng': latlng[1]
        };
        window.plugin.favoritePortals.saveStorage(bookmarkAction, fields);
      }

    } else {
      // If portal isn't saved in bookmarks: Add this bookmark
      bookmarkAction = 'create'
    }

    //if we create or update the portal
    if (['create', 'update'].indexOf(bookmarkAction) != -1) {
      // Get portal name and coordinates
      var p = window.portals[guid];
      var ll = p.getLatLng();
      plugin.favoritePortals.addPortalBookmark(bookmarkAction, guid, level, ll.lat + ',' + ll.lng, p.options.data.title);

      if (bookmarkAction == 'update') {
        var fields = {
          'lat': ll.lat,
          'lng': ll.lng,
          'zones': [
            {'key': getPortalKeyNameFromLevel(currentPortalLevel), 'value': false},
            {'key': getPortalKeyNameFromLevel(level), 'value': true}
          ]
        };

      } else {
        var fields = {
          'guid': guid,
          'name': p.options.data.title,
          'lat': ll.lat,
          'lng': ll.lng,
          'zones': [{'key': getPortalKeyNameFromLevel(level), 'value': true}]
        };
      }
      window.plugin.favoritePortals.saveStorage(bookmarkAction, fields);
    }
  }

  plugin.favoritePortals.addPortalBookmark = function(action, guid, level, latlng, label) {
    var ID = window.plugin.favoritePortals.generateID();

    // Add bookmark in the localStorage
    window.plugin.favoritePortals.bkmrksObj.portals['id' + level]['bkmrk'][ID] = { "guid":guid, "latlng":latlng, "label":label };

    window.plugin.favoritePortals.saveStorage();
    window.plugin.favoritePortals.refreshBkmrks();
    window.runHooks('pluginBkmrksEdit', { "target": "portal", "action": "add", "id": ID, "guid": guid });
  }

  // Remove BOOKMARK
  window.plugin.favoritePortals.removeElement = function(elem) {
    var ID = $(elem).parent('li').attr('id');
    var IDfold = $(elem).parent().parent().parent('li').attr('id');
    var item = window.plugin.favoritePortals.bkmrksObj.portals[IDfold]['bkmrk'][ID]
    var latlng = item.latlng.split(',')

    var fields = {
      'lat': latlng[0],
      'lng': latlng[1]
    };

    delete window.plugin.favoritePortals.bkmrksObj.portals[IDfold]['bkmrk'][ID];
    $(elem).parent('li').remove();

    var list = window.plugin.favoritePortals.bkmrksObj.portals;

    window.plugin.favoritePortals.updateStarsPortal();
    window.plugin.favoritePortals.saveStorage('delete', fields);

    window.runHooks('pluginBkmrksEdit', { "target": "portal", "action": "remove", "folder": IDfold, "id": ID, "guid": item.guid });
    console.log('BOOKMARKS: removed portal (' + ID + ' situated in ' + IDfold + ' folder)');
  }

  window.plugin.favoritePortals.deleteMode = function() {
    $('#bookmarksBox').removeClass('moveMode').toggleClass('deleteMode');
  }

  window.plugin.favoritePortals.moveMode = function() {
    $('#bookmarksBox').removeClass('deleteMode').toggleClass('moveMode');
  }

/***************************************************************************************************************************************************************/

  // Saved the new sort of the bookmarks (in the localStorage)
  window.plugin.favoritePortals.sortBookmark = function() {
    var list = window.plugin.favoritePortals.bkmrksObj.portals;
    var newArr = {};

    $('li.bookmarkFolder').each(function() {
      var idFold = $(this).attr('id');
      newArr[idFold] = window.plugin.favoritePortals.bkmrksObj.portals[idFold];
      newArr[idFold].bkmrk = {};
    });

    $('li.bkmrk').each(function() {
      window.plugin.favoritePortals.loadStorage();

      var idFold = $(this).parent().parent('li').attr('id');
      var id = $(this).attr('id');

      var list = window.plugin.favoritePortals.bkmrksObj.portals;
      for(var idFoldersOrigin in list) {
        for(var idBkmrk in list[idFoldersOrigin]['bkmrk']) {
          if (idBkmrk == id) {
            newArr[idFold].bkmrk[id] = window.plugin.favoritePortals.bkmrksObj.portals[idFoldersOrigin].bkmrk[id];

              if (idFoldersOrigin != idFold) {
              var latlng = window.plugin.favoritePortals.bkmrksObj.portals[idFoldersOrigin].bkmrk[id].latlng.split(',')
              var fields = {
                'lat': latlng[0],
                'lng': latlng[1],
                'zones': [
                  {'key': getPortalKeyNameFromLevel(idFoldersOrigin.slice(2)), 'value': false},
                  {'key': getPortalKeyNameFromLevel(idFold.slice(2)), 'value': true}
                ]
              };
              window.plugin.favoritePortals.saveStorage('update', fields);
            }
          }
        }
      }
    });

    window.plugin.favoritePortals.bkmrksObj.portals = newArr;
    window.plugin.favoritePortals.saveStorage();
    window.plugin.favoritePortals.updateStarsPortal();
    window.runHooks('pluginBkmrksEdit', {"target": "bookmarks", "action": "sort"});
    console.log('BOOKMARKS: sorted bookmark (portal/map)');
  }

  window.plugin.favoritePortals.jquerySortableScript = function() {
    $(".bookmarkList ul li ul").sortable({
      items:"li.bkmrk",
      connectWith:".bookmarkList ul ul",
      handle:".bookmarksLink",
      placeholder:"sortable-placeholder",
      helper:'clone', // fix accidental click in firefox
      forcePlaceholderSize:true,
      update:function(event, ui) {
        window.plugin.favoritePortals.sortBookmark();
      }
    });
  }

/***************************************************************************************************************************************************************/
/** HIGHLIGHTER ************************************************************************************************************************************************/
/***************************************************************************************************************************************************************/
  // window.plugin.favoritePortals.highlight = function(data) {
  //   var guid = data.portal.options.ent[0];
  //   if (window.plugin.favoritePortals.findByGuid(guid)) {
  //     data.portal.setStyle({fillColor:'red'});
  //   }
  // }

  // window.plugin.favoritePortals.highlightRefresh = function(data) {
  //   if (_current_highlighter === 'Bookmarked Portals') {
  //     if (data.target === 'portal' || (data.target === 'folder' && data.action === 'remove') || (data.target === 'all' && data.action === 'import') || (data.target === 'all' && data.action === 'reset')) {
  //       window.resetHighlightedPortals();
  //     }
  //   }
  // }

/***************************************************************************************************************************************************************/
/** BOOKMARKED PORTALS LAYER ***********************************************************************************************************************************/
/***************************************************************************************************************************************************************/
  window.plugin.favoritePortals.addAllStars = function() {
    var list = window.plugin.favoritePortals.bkmrksObj.portals;

    for(var idFolders in list) {
      for(var idBkmrks in list[idFolders]['bkmrk']) {
        var latlng = list[idFolders]['bkmrk'][idBkmrks].latlng.split(",");
        var guid = list[idFolders]['bkmrk'][idBkmrks].guid;
        var lbl = list[idFolders]['bkmrk'][idBkmrks].label;
        window.plugin.favoritePortals.addStar(guid, latlng, lbl);
      }
    }
  }

  window.plugin.favoritePortals.addStar = function(guid, latlng, lbl) {
    var star = L.marker(latlng, {
      title: lbl,
      icon: L.icon({
        iconUrl: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAoCAMAAACo9wirAAAAzFBMVEXbuTR9YwDd0T+ulyry2VDt53/s5Jv34a+tmUbJtGRtVwAtJADg14j304viyIZeSwDXyH5IOQD07JLPvnhqVQBuWACmkjpgTQBxWwC4p1WchikRDQCPdxeBaAQTDwADAgAOCwAAAAAAAAAAAAALCQAAAAAAAAAAAAD9+Jb97IT+3VT9+WH+0i3993D+80793Yr/yXL+5Tr+6XL+zEP/z2X+vSX+zB/+52L/qw393SX89xr9+yv/siP/vUf9+Dv+20P+rgH+xQv+vgH+twNAe/b8AAAAKHRSTlPywvLj+vn4/OLrqFb0/PSS8Hb77meB4FGX5t1A2dUkEy8HDQoaAwEAoclaPQAAAo1JREFUeF5VkeeyozAMRrPphdwkdIxpBu7SSe/llvd/p5VxlMx+PxiMzhxJuPX3mc/PzzgIQwYJwyCGoyggwMuRav6BmKrLGgQBrBvUkuQkkSWLGh5DooX1UNWlXplDyp6kq5EXCqL1qlu9srpfIPeq7FmaIBCIA9WScyjznC/3XOZEEAsABIFBoH6GUrt9v5zPQEAXFoCiAWLPkUqon+8lIbl4kRwfmggABFZSXXh9PhbopUos1fWCBoAJnT4MCMMlZLHQE/Hed/wIFA3gkiSvqjLtK45tO/N+WlZVnhA7YgKAFZK83dGJM5v4rmFSonfaeWotoEcDhJqSVl06W0A5gviTGelWqTJzvZADcWjO11XVIzPbjzzGPFfT4bxWTD8SAANgtTrIQ8eF3QPmDOXDarWej/mUL+C0SgfU91gcR3TAj42BNQDMAMDpwBfzwjCi/cMJgNFMGGBIY5g+AACnrWl+NJay0+mRDhcAiDVdKsOnzJqZRFGoZo74SaYTXDPwzPnhUTw6dCSnaT2inUdRHD7GNgIxM/QUvrW6WfZ4ZFm3VRSiA4sFEEaOlBX7oij2EP4ssqlj413wHqq13uy/X9lvYAcbb5P3cGmd7b9/nvneZzWdNB0EAApN2RZvoNjCvfKb4IBQ+LTe7H5+RXabmjwFCISw6Xb3i8D2oxEgwMdkBgHF7xeEC/SF34yIAN90/LHdHb94QCB2REBsauj17cqB660eogAB8bOE4vgUsBiBt2J6ux6P19v0OQECb8V8y4G34D+AKwgoQEBQgMBLYSrL222pjHEFBN4KOl0up/wWUIAA/k5tNBg014gCBPB3+s5g0EwYYB2Bl8KyND9CAQJvgnmq6kYeXxHzDyATueNnvZcYAAAAAElFTkSuQmCC',
        iconAnchor: [13,35],
        iconSize: [27,36]
      })
    });
    window.registerMarkerForOMS(star);
    star.on('spiderfiedclick', function() { renderPortalDetails(guid); });

    window.plugin.favoritePortals.starLayers[guid] = star;
    star.addTo(window.plugin.favoritePortals.starLayerGroup);
  }

  window.plugin.favoritePortals.editStar = function(data) {
    if (data.action === 'add') {
      var guid = data.guid;
      //var latlng = window.portals[guid].getLatLng();
      //var lbl = window.portals[guid].options.data.title;
      var portalInfos = window.plugin.favoritePortals.findByGuid(guid);
      var portal = window.plugin.favoritePortals.bkmrksObj.portals[portalInfos.id_folder].bkmrk[portalInfos.id_bookmark];
      var latlng = portal.latlng.split(',');
      latlng = new L.LatLng(latlng[0], latlng[1]);
      var label = portal.label;
      var starInLayer = window.plugin.favoritePortals.starLayers[guid];

      window.plugin.favoritePortals.addStar(guid, latlng, label);

    } else if (data.action === 'remove') {
      var starInLayer = window.plugin.favoritePortals.starLayers[data.guid];
      window.plugin.favoritePortals.starLayerGroup.removeLayer(starInLayer);
      delete window.plugin.favoritePortals.starLayers[data.guid];
    }
  }

/***************************************************************************************************************************************************************/

  window.plugin.favoritePortals.setupCSS = function() {
    $('<style>').prop('type', 'text/css').html('/* hide when printing */\n@media print {\n  #bkmrksTrigger { display: none !important; }\n}\n\n\n#bookmarksBox *{\n display:block;\n  padding:0;\n  margin:0;\n width:auto;\n height:auto;\n  font-family:Verdana,Geneva,sans-serif;\n  font-size:13px;\n line-height:22px;\n text-indent:0;\n  text-decoration:none;\n -webkit-box-sizing:border-box;\n  -moz-box-sizing:border-box;\n box-sizing:border-box;\n}\n\n#bookmarksBox{\n display:block;\n  position:absolute !important;\n z-index:4001;\n top:100px;\n  left:100px;\n width:231px;\n  height:auto;\n  overflow:hidden;\n}\n#bookmarksBox .addForm,\n#bookmarksBox #bookmarksTypeBar,\n#bookmarksBox h5{\n height:28px;\n  overflow:hidden;\n  color:#fff;\n font-size:14px;\n}\n#bookmarksBox #topBar{\n  height:15px !important;\n}\n#bookmarksBox #topBar *{\n  height: 14px !important;\n}\n#bookmarksBox #topBar *{\n float:left !important;\n}\n#bookmarksBox .handle{\n width:80%;\n  text-align:center;\n  color:#fff;\n line-height:6px;\n  cursor:move;\n}\n#bookmarksBox #topBar .btn{\n  display:block;\n  width:10%;\n  cursor:pointer;\n color:#20a8b1;\n\n  font-weight:bold;\n text-align:center;\n  line-height:13px;\n font-size:18px;\n}\n\n#bookmarksBox #topBar #bookmarksDel{\n  overflow:hidden;\n  text-indent:-999px;\n background:#B42E2E;\n}\n\n#bookmarksBox #topBar #bookmarksMin:hover{\n  color:#ffce00;\n}\n#bookmarksBox #bookmarksTypeBar{\n clear:both;\n}\n#bookmarksBox h5{\n padding:4px 0 23px 5px;\n color:#ffce00;\n  background:rgba(0,0,0,0);\n}\n#bookmarksBox #topBar,\n#bookmarksBox .addForm,\n#bookmarksBox #bookmarksTypeBar,\n#bookmarksBox .bookmarkList li.bkmrk a,\n#bookmarksBox .bookmarkList li.bkmrk:hover{\n background-color:rgba(8,48,78,.85);\n}\n#bookmarksBox h5,\n#bookmarksBox .bookmarkList li.bkmrk:hover .bookmarksLink,\n#bookmarksBox .addForm *{\n  background:rgba(0,0,0,.3);\n}\n#bookmarksBox .addForm *{\n  display:block;\n  float:left;\n height:28px !important;\n}\n#bookmarksBox .addForm a{\n cursor:pointer;\n color:#20a8b1;\n  font-size:12px;\n width:35%;\n  text-align:center;\n  line-height:20px;\n padding:4px 0 23px;\n}\n#bookmarksBox .addForm a:hover{\n background:#ffce00;\n color:#000;\n text-decoration:none;\n}\n#bookmarksBox .addForm input{\n font-size:11px !important;\n  color:#ffce00;\n  height:28px;\n  padding:4px 8px 1px;\n  line-height:12px;\n font-size:12px;\n}\n#bookmarksBox #bkmrk_portals .addForm input{\n  width:65%;\n}\n#bookmarksBox #bkmrk_maps .addForm input{\n  width:42%;\n}\n#bookmarksBox #bkmrk_maps .addForm a{\n  width:29%;\n}\n#bookmarksBox .addForm input:hover,\n#bookmarksBox .addForm input:focus{\n outline:0;\n  background:rgba(0,0,0,.6);\n}\n#bookmarksBox .bookmarkList > ul{\n  clear:both;\n list-style-type:none;\n color:#fff;\n overflow:hidden;\n  overflow-y:auto;\n  max-height:580px;\n}\n#bookmarksBox .sortable-placeholder{\n  background:rgba(8,48,78,.55);\n box-shadow:inset 1px 0 0 #20a8b1;\n}\n#bookmarksBox .ui-sortable-helper{\n  border-top-width:1px;\n}\n#bookmarksBox .bookmarkList.current{\n  display:block;\n}\n#bookmarksBox h5,\n#bookmarksBox .addForm *,\n#bookmarksBox ul li.bkmrk,\n#bookmarksBox ul li.bkmrk a{\n height:22px;\n}\n#bookmarksBox ul li.bkmrk a{\n  overflow:hidden;\n  cursor:pointer;\n float:left;\n}\n#bookmarksBox ul .bookmarksRemoveFrom{\n  width:10%;\n  text-align:center;\n  color:#fff;\n}\n#bookmarksBox ul .bookmarksLink{\n  width:90%;\n  padding:0 10px 0 8px;\n color:#ffce00;\n}\n#bookmarksBox ul .bookmarksLink.selected{\n  color:#03fe03;\n}\n#bookmarksBox ul .bookmarksLink:hover{\n color:#03fe03;\n}\n#bookmarksBox ul .bookmarksRemoveFrom:hover{\n color:#fff;\n background:#e22 !important;\n}\n\n/*---- UI border -----*/\n#bookmarksBox,\n#bookmarksBox *{\n  border-color:#20a8b1;\n border-style:solid;\n border-width:0;\n}\n#bookmarksBox #topBar,\n#bookmarksBox ul .bookmarkFolder{\n border-top-width:1px;\n}\n\n#bookmarksBox #topBar,\n#bookmarksBox #bookmarksTypeBar,\n#bookmarksBox .addForm,\n#bookmarksBox ul .bookmarkFolder .folderLabel,\n#bookmarksBox ul li.bkmrk a {\n  border-bottom-width:1px;\n}\n#bookmarksBox ul .bookmarkFolder{\n  border-right-width:1px;\n border-left-width:1px;\n}\n#bookmarksBox #topBar *,\n#bookmarksBox #bookmarksTypeBar *,\n#bookmarksBox .addForm *,\n#bookmarksBox ul li.bkmrk{\n  border-left-width:1px;\n}\n#bookmarksBox #topBar,\n#bookmarksBox #bookmarksTypeBar,\n#bookmarksBox .addForm,\n#bookmarksBox ul .bookmarksRemoveFrom{\n  border-right-width:1px;\n}\n#bkmrksTrigger{\n  display:block;\n  position:absolute;\n  overflow:hidden;\n  top:0;\n  left:277px;\n width:47px;\n margin-top:-36px;\n height:64px;\n  height:0;\n cursor:pointer;\n z-index:2999;\n background-position:center bottom;\n  background-repeat:no-repeat;\n  transition:margin-top 100ms ease-in-out;\n  text-indent:-100%;\n  text-decoration:none;\n text-align:center;\n}\n#bkmrksTrigger:hover{\n  margin-top:0;\n}\n#sidebar #portaldetails h3.title{\n width:auto;\n}\n.portal-list-bookmark span {\n  display:inline-block;\n margin: -3px;\n width:16px;\n height:15px;\n  overflow:hidden;\n  background-repeat:no-repeat;\n  cursor:pointer;\n}\n.bkmrksStar{\n  display:inline-block;\n}\n#bkmrksTrigger, .bkmrksStar span, .portal-list-bookmark span {\n  background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC8AAABPCAMAAABMDWzEAAAANlBMVEX/////zgD/zgD///////8Aru7/zgAAru4TtPAAAADA7PtAwvLk9/6b3/n///8Aru510/b/zgDZKp6YAAAACnRSTlOAxo5FtDw9mPoA9GJiegAAAklJREFUeF6dle26ozAIhFO1NkK+vP+b3WbBJRwM7dn5lad9BweoaThI63Z42hfmLn4rLv84d8WvpWxe+fNcFL+VUtzy57kLv67lrbDOqu/nW8tfQ1i3MmjbfrKPc9BjCYfiy2qjjNoDZRfcaBnxnl8Mm8KN4bFzv6q6lVT/P369+DBZFmsZ+LAmWbHllz7XB/OBwDDhF1rVIvwFhHt+vw4dqbViKdC0wHySSsE3e/FxpHPpAo+vUehUSCk7PBuYTpCUw/JsAIoipzlfUTHimPGNMujQ7LA86sSqm2x4BFXbOjTPSWJFxtgpbRTFd+VITdPGQG3b8hArCbm7n9vVefqZxT8I0G2Y+Yi4XFNy+Jqpn695WlP6ksdWSJB9PmJrkMqolADyjIdyrzSrD1Pc8lND8vrNFvfnkw3u8NYAn+ev+M/7iorPH3n8Jd9+mT+b8fg8EBZb+o4n+n0gx4yPMp5MZ3LkW77XJAaZZkdmPtv7JGG9EfLLrnkS3DjiRWseej6OrnXd0ub/hQbftIPHCnfzjDz6sXjy3seKoBqXG97yqiCgmFv198uNYy7XptHlr8aHcbk8NW5veMtrg+A1Ojy3oCeLDs9zgfEHEi2vu03INu4Y/fk3OVOo6N2f8u5IqDs+NvMaYOJQaHj5rut1vGIda/zk5dmdfh7H8XypUJpP0luNne56xnEdildRRPyIfMMDSnGWhEJQvEQZittQwoONYkP946OOMnsERuZNFKMXOYiXkXsO4U0UL1QwffqPCH4Us4xgovih/gBs1LqNE0afwAAAAABJRU5ErkJggg==);\n}\n.bkmrksStar span{\n display:inline-block;\n float:left;\n margin:3px 1px 0 4px;\n width:16px;\n height:15px;\n  overflow:hidden;\n  background-repeat:no-repeat;\n}\n.bkmrksStar span, .bkmrksStar.favorite:focus span{\n background-position:left top;\n}\n.bkmrksStar:focus span, .bkmrksStar.favorite span, .portal-list-bookmark.favorite span{\n background-position:right top;\n}\n#bookmarksBox .bookmarkList .bookmarkFolder{\n overflow:hidden;\n  margin-top:-1px;\n  height:auto;\n  background:rgba(8,58,78,.7);\n}\n#bookmarksBox .bookmarkList ul li.sortable-placeholder{\n  box-shadow:inset -1px 0 0 #20a8b1,inset 1px 0 0 #20a8b1,0 -1px 0 #20a8b1;\n background:rgba(8,58,78,.9);\n}\n#bookmarksBox .bookmarkList .bkmrk.ui-sortable-helper{\n border-right-width:1px;\n border-left-width:1px !important;\n}\n#bookmarksBox .bookmarkList ul li ul li.sortable-placeholder{\n height:23px;\n  box-shadow:inset 0 -1px 0 #20a8b1,inset 1px 0 0 #20a8b1;\n}\n\n#bookmarksBox .bookmarkList ul li.bookmarkFolder.ui-sortable-helper{\n  box-shadow:inset 0 -1px 0 #20a8b1;\n}\n\n#bookmarksBox #topBar #bookmarksDel,\n#bookmarksBox .bookmarkList .bookmarkFolder .folderLabel:hover .bookmarksRemoveFrom,\n#bookmarksBox .bookmarkList .bookmarkFolder .folderLabel:hover .bookmarksAnchor{\n border-bottom-width:1px;\n}\n\n/*---------*/\n#bookmarksBox .bookmarkList .bookmarkFolder .folderLabel .bookmarksAnchor span,\n#bookmarksBox .bookmarkList .bookmarkFolder .folderLabel > span,\n#bookmarksBox .bookmarkList .bookmarkFolder .folderLabel > span > span,\n#bookmarksBox .bookmarkList .triangle{\n  width:0;\n  height:0;\n}\n#bookmarksBox .bookmarkList .bookmarkFolder .folderLabel{\n overflow:visible;\n height:25px;\n  cursor:pointer;\n background:#069;\n  text-indent:0;\n}\n#bookmarksBox .bookmarkList .bookmarkFolder .folderLabel > *{\n  height:25px;\n  float:left;\n}\n#bookmarksBox .bookmarkList .bookmarkFolder .folderLabel .bookmarksAnchor{\n  line-height:25px;\n color:#fff;\n width:90%;\n}\n#bookmarksBox .bookmarkList .bookmarkFolder .folderLabel .bookmarksAnchor span{\n  float:left;\n border-width:5px 0 5px 7px;\n border-color:transparent transparent transparent white;\n margin:7px 7px 0 6px;\n}\n#bookmarksBox .bookmarkList .bookmarkFolder.open .folderLabel .bookmarksAnchor span{\n  margin:9px 5px 0 5px;\n border-width:7px 5px 0 5px;\n border-color:white transparent transparent transparent;\n}\n#bookmarksBox .bookmarkList .bookmarkFolder .folderLabel > span,\n#bookmarksBox .bookmarkList .bookmarkFolder .folderLabel > span > span{\n display:none;\n border-width:0 12px 10px 0;\n border-color:transparent #20a8b1 transparent transparent;\n margin:-20px 0 0;\n position:relative;\n  top:21px;\n left:219px;\n}\n#bookmarksBox .bookmarkList .bookmarkFolder .folderLabel > span > span{\n top:18px;\n left:0;\n border-width:0 10px 9px 0;\n  border-color:transparent #069 transparent transparent;\n}\n#bookmarksBox .bookmarkList .bookmarkFolder.open .folderLabel > span,\n#bookmarksBox .bookmarkList .bookmarkFolder.open .folderLabel > span > span{\n  display:block;\n  display:none;\n}\n#bookmarksBox .bookmarkList .bookmarkFolder.open .folderLabel:hover > span > span{\n  border-color:transparent #036 transparent transparent;\n}\n#bookmarksBox .bookmarkList .bookmarkFolder .folderLabel:hover .bookmarksAnchor{\n background:#036;\n}\n#bookmarksBox .bookmarkList .bookmarkFolder ul{\n  display:none;\n margin-left:10%;\n}\n#bookmarksBox .bookmarkList .bookmarkFolder.open ul{\n display:block;\n  min-height:22px;\n}\n/*---- Width for deleteMode -----*/\n#bookmarksBox .bookmarksRemoveFrom{\n display:none !important;\n}\n#bookmarksBox.deleteMode .bookmarksRemoveFrom{\n display:block !important;\n}\n\n#bookmarksBox .bookmarkList .bookmarkFolder .folderLabel .bookmarksAnchor,\n#bookmarksBox ul .bookmarksLink{\n width:100% !important;\n}\n#bookmarksBox.deleteMode ul .bookmarksLink{\n width:90% !important;\n}\n/*--- Other Opt css ---*/\n\n#bookmarksBox.moveMode ul .bookmarksLink{\n width:90% !important;\n}\n').appendTo('head');
  }

  window.plugin.favoritePortals.htmlStars = function() {
    html = ''
    for (var i=3; i>=1; i--) {
      html += window.plugin.favoritePortals.htmlStar(i);
    }

    return html;
  }

  window.plugin.favoritePortals.htmlStar = function(level) {
    return'<a class="bkmrksStar" onclick="window.plugin.favoritePortals.switchStarPortal(' + level + ');return false;" title="Save this portal in your bookmarks"><span></span></a>';
  }

  window.plugin.favoritePortals.setupContent = function() {
    plugin.favoritePortals.htmlBoxTrigger = '<a id="bkmrksTrigger" class="open" onclick="window.plugin.favoritePortals.switchStatusBkmrksBox(\'switch\');return false;" accesskey="v" title="[v]">[-] Bookmarks</a>';
    plugin.favoritePortals.htmlBkmrksBox = '<div id="bookmarksBox">'
                           + '<div id="topBar">'
                             + '<a id="bookmarksMin" class="btn" onclick="window.plugin.favoritePortals.switchStatusBkmrksBox(0);return false;" title="Minimize">-</a>'
                             + '<div class="handle">...</div>'
                             + '<a id="bookmarksDel" class="btn" onclick="window.plugin.favoritePortals.deleteMode();return false;" title="Show/Hide \'X\' button">Show/Hide "X" button</a>'
                           + '</div>'
                           + '<div id="bookmarksTypeBar">'
                             + '<h5 class="bkmrk_portals">Portals</h5>'
                           + '</div>'
                           + '<div id="bkmrk_portals" class="bookmarkList">'
                           + '</div>'
                           + '<div style="border-bottom-width:1px;"></div>'
                         + '</div>';

    plugin.favoritePortals.htmlDisabledMessage = '<div title="Your browser do not support localStorage">Plugin Bookmarks disabled*.</div>';
    plugin.favoritePortals.htmlStars = window.plugin.favoritePortals.htmlStars();
    plugin.favoritePortals.htmlMoveBtn = '<a id="bookmarksMove" class="btn" onclick="window.plugin.favoritePortals.moveMode();return false;">Show/Hide "Move" button</a>'
  }

/***************************************************************************************************************************************************************/

  var launchSetup = function() {
    setupHasBeenLaunched = true;
    // Fired when a bookmark is removed, added or sorted, also when a folder is opened/closed.
    if ($.inArray('pluginBkmrksEdit', window.VALID_HOOKS) < 0) { window.VALID_HOOKS.push('pluginBkmrksEdit'); }

    // If the storage not exists or is a old version
    window.plugin.favoritePortals.createStorage();

    // Load data from localStorage
    window.plugin.favoritePortals.loadStorage(true);
    window.plugin.favoritePortals.loadStorageBox();
    window.plugin.favoritePortals.setupContent();
    window.plugin.favoritePortals.setupCSS();

    $('body').append(window.plugin.favoritePortals.htmlBoxTrigger + window.plugin.favoritePortals.htmlBkmrksBox);
    $('#bookmarksBox').draggable({ handle:'.handle', containment:'window' });
    $("#bookmarksBox #bookmarksMin , #bookmarksBox ul li, #bookmarksBox ul li a, #bookmarksBox ul li a span, #bookmarksBox h5, #bookmarksBox .addForm a").disableSelection();
    $('#bookmarksBox').css({'top':window.plugin.favoritePortals.statusBox.pos.x, 'left':window.plugin.favoritePortals.statusBox.pos.y});

    window.plugin.favoritePortals.loadList();
    window.plugin.favoritePortals.jquerySortableScript();

    if (window.plugin.favoritePortals.statusBox['show'] === 0) { window.plugin.favoritePortals.switchStatusBkmrksBox(0); }
    if (window.plugin.favoritePortals.statusBox['page'] === 1) { $('#bookmarksBox h5.bkmrk_portals').trigger('click'); }

    window.addHook('portalSelected', window.plugin.favoritePortals.onPortalSelected);

    // Highlighter - bookmarked portals
    //window.addHook('pluginBkmrksEdit', window.plugin.favoritePortals.highlightRefresh);
    //window.addPortalHighlighter('Bookmarked Portals', window.plugin.favoritePortals.highlight);

    // Layer - Bookmarked portals
    window.plugin.favoritePortals.starLayerGroup = new L.LayerGroup();
    window.addLayerGroup('My favorite portals', window.plugin.favoritePortals.starLayerGroup, false);
    window.plugin.favoritePortals.addAllStars();
    window.addHook('pluginBkmrksEdit', window.plugin.favoritePortals.editStar);

    window.addHook('mapDataRefreshStart', function(data) {
      window.plugin.favoritePortals.deleteAllMarkers();
      window.plugin.favoritePortals.addPortalsHighlights();
    });
  };

  var loginEvent = function() {
    Parse.User.logIn(window.plugin.favoritePortals.playerName, $('#login-password-favorite-portals').val(), {
      success: function(user) {
        $('#login-form-favorite-portals').remove();
        if (timerLaunch !== undefined && !setupHasBeenLaunched) {
          launchSetup();
          clearTimeout(timerLaunch);
        }
      },
      error: function(user, error) {
        $('#login-form-favorite-portals .error').html("Invalid username or password.<br>Please try again or contact the administrator").show();
      }
    });
  }

  var setup = function() {
    timerParseIntervalId = setInterval(function() {
      if (Parse !== undefined) {
        clearInterval(timerParseIntervalId);
        Parse.initialize("iM1qF0BoTL8ypYC4iREBAD3vTULI08mkrPCbtwiF", "7QcgHKFwix8XWO59NrY0StmJ3XyEyK4o8z0SMwtj");

        if (!Parse.User.current()) {
          htmlForm = '<div id="login-form-favorite-portals" style="position: absolute; top: 20%; background-color:rgba(8, 48, 78, 0.901961); padding: 10px; left: 35%; border: 1px solid #20A8B1; z-index: 9999;">' +
                         '<div header>' +
                           '<h4 style="color: #ffce00; display: inline-block; margin:0; margin-bottom: 15px; padding-right: 15px;">Se connecter au plugin de portails favoris</h4>' +
                           '<a style="display: inline-block; 1px solid #ffce00; padding: 0 5px;" id="cancel-login-form-favorite-portals">X</span>' +
                         '</div>' +
                         '<div class="error" style="display:none; color: #fff; margin-bottom: 15px;"></div>' +
                         '<input type="password" id="login-password-favorite-portals" placeholder="Password" />' +
                         '<span id="button-login-form-favorite-portals" style="display: inline-block; margin-left: 5px; border: 1px solid #20A8B1; padding: 2px; background-color: rgba(8, 48, 78, 0.901961); color: #20A8B1;">Log In</span>' +
                       '</div>';

            $('body').append(htmlForm);

            // events

            // cancel
            $('#cancel-login-form-favorite-portals').on('click', function(){
              $('#login-form-favorite-portals').remove();
            })

            // login - click
            $('#button-login-form-favorite-portals').on('click', function() {
              loginEvent();
              return false;
            });

            // login - enter
            $("#login-password-favorite-portals").keypress(function(e) {
              if((e.keyCode ? e.keyCode : e.which) !== 13) return;
              loginEvent();
              return false;
            });
        }

        window.addHook('mapDataRefreshEnd', function(data) {
          timerLaunch = setTimeout(function() {
            if (Parse.User.current() && !setupHasBeenLaunched) {
              launchSetup();
            }
          }, 0);
        });
      }
    }, 200);
  }

// PLUGIN END //////////////////////////////////////////////////////////


setup.info = plugin_info; //add the script info data to the function as a property
if (!window.bootPlugins) window.bootPlugins = [];
window.bootPlugins.push(setup);
// if IITC has already booted, immediately run the 'setup' function
if (window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end

// inject code into site context
var script = document.createElement("script");
    script.src = "//www.parsecdn.com/js/parse-1.3.4.min.js";
(document.body || document.head || document.documentElement).appendChild(script);

var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('(' +  wrapper  + ')(' + JSON.stringify(info) + ');'));
(document.body || document.head || document.documentElement).appendChild(script);

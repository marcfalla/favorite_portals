// ==UserScript==
// @id          iitc-plugin-favorite-portals-test@Mushovelik
// @name        IITC plugin: Favorite portals test
// @author      Mushovelik
// @category    Controls
// @namespace   https://www.ingress.com/intel*
// @version     0.1
// @description this does nothing but giving a good example
// @include     https://www.ingress.com/intel*
// @include     http://www.ingress.com/intel*
// @match       https://www.ingress.com/intel*
// @match       http://www.ingress.com/intel*
// @require   file:///Users/marcfalla/Documents/ingress/favorite_portals/favorite_portals.js
// @grant       none
// ==/UserScript==
